﻿using Emgu.CV;
using Emgu.CV.OCR;
using Emgu.CV.Structure;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TextFromImage
{
    public partial class Form1 : Form
    {
        private string filePath = string.Empty;
        private string language = string.Empty;

        public Form1()
        {
            InitializeComponent();
        }

        private void открытьToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                filePath = openFileDialog1.FileName;
                pictureBox1.Image = Image.FromFile(filePath);
            }
            else
                MessageBox.Show("Неудалось открыть картинку(видимо)", "Ошибка", MessageBoxButtons.OK);
        }

        private void toolStripButton1_Click(object sender, EventArgs e)
        {
            if (String.IsNullOrEmpty(filePath) || String.IsNullOrWhiteSpace(filePath))
                MessageBox.Show("Картинка не выбрана(видимо)", "Ошибка", MessageBoxButtons.OK);
            else if (toolStripComboBox1.SelectedItem == null)
                MessageBox.Show("Язык не выбран(видимо)", "Ошибка", MessageBoxButtons.OK);
            else
            {
                try
                {
                    var path = Path.GetFullPath(@"data");
                    var tesseract = new Tesseract(path, language, OcrEngineMode.TesseractLstmCombined);
                    tesseract.SetImage(new Image<Bgr, byte>(filePath));
                    tesseract.Recognize(); 
                    richTextBox1.Text = tesseract.GetUTF8Text();
                    tesseract.Dispose();
                }
                catch
                {
                    MessageBox.Show("Походу че-то серьезное", "Ошибка", MessageBoxButtons.OK);
                    Application.Exit();
                }
            }    
        }

        private void toolStripComboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (toolStripComboBox1.SelectedIndex == 0)
                language = "rus";
            else if (toolStripComboBox1.SelectedIndex == 1)
                language = "eng";
        }
    }
}
